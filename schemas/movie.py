from pydantic import BaseModel,Field
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(max_length = 15)
    overview: str = Field(default="Descripcion de la pelicula", min_length = 3, max_length= 100)
    year: int  = Field(le=2024)
    rating: float = Field(ge= 1, le=10)
    category: str = Field(min_length= 5, max_length= 15)

    class Config: 
        json_schema_extra = {
            "example": {
                "id":0,
                "title": "string",
                "overview": "string",
                "year": 2008,
                "rating": 0.0,
                "category": "string"
            }
        }