from fastapi import FastAPI, Body, Path, Query
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List

app = FastAPI()
app.title="Proyecto 1er Parcial"
app.version="0.0.1"

#diccionario en una sola cuartilla se entrega el trbajao , el pensamiento de cada autor 
libros=[
    {
        "titulo":"Avatar",
        "autor": "ELeonardoPozos",
        "año": "2009",
        "categoria": "acción",
        "codigo": 1,
        "numeroPaginas":20
    }
]


@app.get('/libros', tags=['libros'])
def get_libros():
    return libros


@app.get('/libros/{id}', tags=['libros'])
def get_libro(codigo: int):
    for item in libros:
        if item["codigo"] == codigo:
            return item
    return []
    
# @app.get('/movies/', tags=['movies'])
# def get_movies_by_category(category: str = Query(min_length=5, max_length=15)):
#     return [item for item in movies if item['category'] == category]                 ES LO MISMO DE ABABJO

# @app.get('/libros/{categoria}', tags=['libros'])
# def get_libros_by_category(category: str):
#     alfred = [item for item in movies if item['category'] == category]
#     return JSONResponse(status_code=200 ,content=alfred)

@app.get('/libros/categoria/', tags=['libros'])
def get_libros_by_category(categoria: str):
    lst = []
    for item in libros:
        if item["categoria"] == categoria:
            lst.append(item)
    return lst

# @app.post('/movies/', tags=['movies'])
# def create_movie(movie : Movie):
#     movies.append(movie)
#     return movies

# @app.post('/movies/', tags=['movies'], response_model=dict, status_code=200)
# def create_movie(movie : Movie) -> dict:
#     movies.append(movie)
#     return JSONResponse(status_code=200 ,content={"message": "Se ha registrado la pelicula"})

@app.post('/libros/', tags=['libros'])
def create_pelis(numeroPaginas: int = Body(), titulo: str = Body(), autor: str = Body()
, año: int = Body(), categoria: str = Body(), codigo: int = Body()):
    libros.append(
        {  
        "titulo":titulo,
        "autor": autor,
        "año": año,
        "categoria": categoria,
        "codigo": codigo,
        "numeroPaginas":numeroPaginas
        }
    ) 
    return libros

# @app.delete('/movies/', tags=['movies'])
# def delete_movie(id: int):
#     for item in movies:
#         if item["id"] == id:
#             movies.remove(item)
#     return movies 

# @app.delete('/movies/', tags=['movies'], response_model=dict, status_code=200)
# def delete_movie(id: int):
#     for item in movies:
#         if item["id"] == id:
#             movies.remove(item)
#     return JSONResponse(status_code=200 ,content={"message": "Se ha eliminado la pelicula"}) 

@app.delete('/libros/', tags=['libros'])
def delete_libro(codigo: int):
    for item in libros:
        if item["codigo"] == codigo:
            libros.remove(item)
    return libros

# @app.put('/movies/', tags=['movies'])
# def update_movie(id: int, movie : Movie):
#     for movie in movies:
#         if movie["id"] == id:
#             movie["title"] = movie.title 
#             movie["overview"] = movie.overview
#             movie["year"] = movie.year
#             movie["rating"] = movie.rating 
#             movie["category"] = movie.category
#             return movies
#     return {"error": "La película con el ID proporcionado no fue encontrada"}


# @app.put('/movies/', tags=['movies'], response_model=dict, status_code=200)
# def update_movie(id: int, movie : Movie) -> dict:
#     for movie in movies:
#         if movie["id"] == id:
#             movie["title"] = movie.title
#             movie["overview"] = movie.overview
#             movie["year"] = movie.year
#             movie["rating"] = movie.rating 
#             movie["category"] = movie.category
#             return movies
#     return JSONResponse(status_code=200 ,content={"message": "Se ha modificado la pelicula"})

@app.put('/libros/', tags=['libros'])
def update_libro( codigo: int, numeroPaginas: int = Body(), titulo: str = Body(), autor: str = Body()
                , año: str = Body(), categoria: str = Body()):
    for libro in libros:
        if libro["codigo"] == codigo:
            libro["titulo"] = titulo
            libro["autor"] = autor
            libro["año"] = año
            libro["categoria"] = categoria
            libro["numeroPaginas"] = numeroPaginas
            return libros

# #--------------------------------------------------------------------------------

# @app.get('/compus', tags=['compus'])
# def get_compu():
#     return compus

# @app.get('/compus/{id}', tags=['compus'])
# def get_compus(id: int):
#     for item in compus:
#         if item["id"] == id:
#             return item
#     return [] 

# @app.get('/compus/{marca}/', tags=['compus'])
# def get_compus(marca: str):
#     lst = []
#     for item in compus:
#         if item["marca"] == marca:
#             lst.append(item)
#     return lst



# @app.post('/compus/', tags=['compus'])
# def create_compu(id: int = Body(), marca: str = Body(), modelo: str = Body(), color: str = Body(), RAM: str = Body(), almacenamiento: int = Body()):
#     compus.append({
#         "id": id,
#         "marca": marca,
#         "modelo": modelo,
#         "color": color,
#         "RAM": RAM,
#         "almacenamiento": almacenamiento
#     })
#     return compus

# @app.delete('/compus/', tags=['compus'])
# def delete_compu(id: int):
#     for item in compus:
#         if item["id"] == id:
#             compus.remove(item)
#     return compus

# @app.put('/compus/', tags=['compus'])
# def update_compu(id: int, marca: str = Body(), modelo: str = Body(), color: str = Body(), RAM: str = Body(), almacenamiento: int = Body()):
#     for compu in compus:
#         if compu["id"] == id:
#             compu["marca"] = marca
#             compu["modelo"] = modelo
#             compu["color"] = color
#             compu["RAM"] = RAM
#             compu["almacenamiento"] = almacenamiento
#             return compus
#     return {"error": "La computadora con el ID proporcionado no fue encontrada"}

# @app.put('/compus/', tags=['compus'])
# def update_compu(id: int, marca: str = Body(), modelo: str = Body(), color: str = Body(), RAM: str = Body(), almacenamiento: int = Body()):
#     """
#     Esta función actualiza los datos de una computadora existente basada en su ID.

#     Parámetros:
#     - id: El identificador único de la computadora a actualizar.
#     - marca: La nueva marca de la computadora.
#     - modelo: El nuevo modelo de la computadora.
#     - color: El nuevo color de la computadora.
#     - RAM: La nueva cantidad de RAM de la computadora.
#     - almacenamiento: La nueva capacidad de almacenamiento de la computadora.
#     """
#     # Se recorre la lista de computadoras
#     for compu in compus:
#         # Se comprueba si el ID de la computadora actual coincide con el ID proporcionado
#         if compu["id"] == id:
#             # Se actualizan los atributos de la computadora con los valores proporcionados
#             compu["marca"] = marca
#             compu["modelo"] = modelo
#             compu["color"] = color
#             compu["RAM"] = RAM
#             compu["almacenamiento"] = almacenamiento
#             # Se devuelve la lista de computadoras actualizada
#             return compus
#     # Si no se encuentra ninguna coincidencia, se devuelve un mensaje de error
#     return {"error": "La computadora con el ID proporcionado no fue encontrada"}
    

#realiza los aintpoints para  con una lista de 5 registros con los siguientes valores , id, marca , modelo, color, RAM y almacenamiento; en lugar de by categoria sera get by marca; en lugar de mostrar una 
#; de manera que se obtenga todo el cuerpo por la marca ; en caso de ser una mostrara todas


#Realiza los endpoint para la venta de computadoras con una lista de 5 registros con los siguientes valores:
# id, marca, modelo, color, ram y almacenamiento.
#Get by marca de manera que muestre toda la informarcion de la computadora por la marca