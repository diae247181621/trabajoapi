from sqlalchemy import Column, Integer, String
from config.database import Base

class Computer(Base):
    __tablename__ = "computers"

    id = Column(Integer, primary_key = True)
    marca = Column(String)
    modelo = Column(String)
    color = Column(String)
    ram = Column(Integer)
    almacenamiento = Column(Integer)







    