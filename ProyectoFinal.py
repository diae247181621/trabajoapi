from fastapi import FastAPI, Body, Path, Query
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List

app = FastAPI()
app.title="Proyecto 1er Parcial"
app.version="0.0.1"

codigo = 0
id = 0

class Categoria(BaseModel):
    id: Optional[int] = None
    categoria: str = Field(min_length = 4, max_length = 15)

    class Config:
        json_schema_extra = {
            "example": {
                "categoria": "Nombre de la categoría"
            }
        }

categorias = []

@app.get('/categorias', tags=['Categorias'], response_model=List[Categoria], status_code=200)
def get_all_categorias():
    return JSONResponse(content=categorias, status_code=200)

@app.post('/categorias/agregar/', tags=['Categorias'], response_model=dict, status_code=200)
def create_categoria(categoria: Categoria) -> dict:
    global id
    id += 1 
    categoria.id = id
    categoria_dict = categoria.dict()
    categorias.append(categoria_dict)
    return JSONResponse(content={"message": "Se ha registrado exitosamente", "id_asignado": categoria.id})

@app.put('/categorias/actualizar/', tags=['Categorias'], response_model=dict, status_code=200)
def update_categoria(id: int, categoria: Categoria):
    for item1 in categorias:
        if item1["id"] == id:
            categoria_a_cambiar = item1["categoria"]
            item1["categoria"] = categoria.categoria
            for item2 in libros:
                if item2["categoria"] == categoria_a_cambiar:
                    item2["categoria"] = categoria.categoria
            return JSONResponse(content={"message": "Se actualizo exitosamente"}, status_code=200)
    return JSONResponse(content={"message":"No se encontro ninguna categoria con ese id"}, status_code=400)

@app.delete('/categorias/eliminar/', tags=['Categorias'], response_model=dict, status_code=200)
def delete_categoria(id: int) -> dict:
    for item in categorias:
        if item["id"] == id:
            categoria_a_eliminar = item["categoria"]
            for item2 in libros:
                if item2["categoria"] == categoria_a_eliminar:
                    return JSONResponse(content={"message":"Tiene un libro registrado con esta categoria."})
            categorias.remove(item)
            return JSONResponse(content={"message": "Se elimino exitosamente"}, status_code=200)
    return JSONResponse(content={"message":"No se encontro ninguna categoria con ese id"}, status_code=400)



class Libro(BaseModel):
	codigo: Optional[int] = None
	titulo: str = Field(min_length=5, max_length=25)
	autor : str = Field(min_length=5, max_length=20)
	año: int = Field(le=2024)
	categoria: str = Field(min_length=5, max_length=15)
	numeroPaginas: int = Field(ge=49, le=1500)

	class Config:
		json_schema_extra = {
			"example": {
				"titulo": "Titulo del libro",
				"autor": "Autor del libro",
				"año": 1234,
				"categoria": "Categoría del Libro",
				"numeroPaginas": 123
			}
		}

libros = []

@app.get('/libros', tags=['Libros'], response_model=List[Libro], status_code=200)
def get_libros() -> List[Libro]:
    return JSONResponse(content=libros, status_code=200)

@app.get('/libros/{codigo}', tags=['Libros'], status_code=200)
def get_libro(codigo: int = Path(ge=1, le=100)):
    for item in libros:
        if item["codigo"] == codigo:
            return JSONResponse(status_code=200, content=item)
    return JSONResponse(content={"message":"No se encontro ningun libro con ese codigo"}, status_code=400)

@app.get('/libros/categoria/', tags=['Libros'], response_model=List[Libro], status_code=200)
def get_libros_by_category(categoria: str = Query(min_length=5, max_length=15)):
    paJSON = [item for item in libros if item["categoria"] == categoria]
    return JSONResponse(content=paJSON, status_code=200)

@app.post('/libros/registrar/', tags=['Libros'], response_model=dict, status_code=200)
def create_libro(libro: Libro) -> dict:
    for item in categorias:
        if item['categoria'] == libro.categoria:
            global codigo
            codigo += 1
            libro.codigo = codigo
            libro_dict = libro.dict()
            libros.append(libro_dict)
            return JSONResponse(content={"message": "Se ha registrado exitosamente", "codigo_asignado": libro.codigo})
    return JSONResponse(content={"message":"Categoria no registrada"}, status_code=400)

@app.delete('/libros/eliminar/', tags=['Libros'], response_model=dict, status_code=200)
def delete_libro(codigo: int) -> dict:
    for item in libros:
        if item["codigo"] == codigo:
            libros.remove(item)
            return JSONResponse(content={"message":"Se ha elimiando exitosamente"}, status_code=200)
    return JSONResponse(content={"message":"No se encontro ningun libro con ese codigo"}, status_code=400)

@app.put('/libros/actualizar/', tags=['Libros'], response_model=dict, status_code=200)
def update_libro(codigo: int, libro: Libro):
    for item1 in libros:
        if item1["codigo"] == codigo:
            for item2 in categorias:
                if item2["categoria"] == libro.categoria:
                    item1["titulo"] = libro.titulo
                    item1["autor"] = libro.autor
                    item1["año"] = libro.año
                    item1["categoria"] = libro.categoria
                    item1["numeroPaginas"] = libro.numeroPaginas
                    return JSONResponse(content={"message":"Se a realizado la modficación exitosamente"}, status_code=200)
            return JSONResponse(content={"message":"No esta registrada esa categoria"}, status_code=400)
    return JSONResponse(content={"message":"No se encontro ningun libro con ese codigo"}, status_code=400)