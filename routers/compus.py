from fastapi import Path, Depends
from fastapi.responses import  JSONResponse
from pydantic import BaseModel, Field
from typing import  Optional, List
from config.database import Session
from middlewares.jwt_bearer import JWTBearer
from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from models.compu import Computer as ComputerModel

compus_router = APIRouter()

class Computer(BaseModel):
    id: Optional[int] = None
    marca: str = Field(max_length=15)
    modelo: str = Field(max_length=15)
    color: str = Field(max_length=15)
    ram: int
    almacenamiento: int

    class Config: 
        json_schema_extra = {
            "example": {
                "marca":"Mi pelicula",
                "modelo": "Descripcion de la pelicula",
                "color": "2024",
                "ram": 0,
                "almacenamiento": 0
            }
        }


@compus_router.get('/compus', tags=['compus'], response_model=None, status_code=200)
async def get_compus():
    db = Session()
    result = db.query(ComputerModel).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@compus_router.get('/compus/{id}', tags=['compus'], response_model=Computer, status_code=200)
def get_compu(id: int = Path(ge=1, le=2000)) -> Computer:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if result:
        return JSONResponse(status_code=200, content=jsonable_encoder(result))
    else:
        return JSONResponse(status_code=404, content={'message: "No encontrado'})
    
@compus_router.get('/compus{marca}', tags=['compus'], response_model=List[Computer], status_code=200)
def get_compus_by_marca(marca: str) -> List[Computer]:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.marca == marca).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@compus_router.post('/compus/', tags=['compus'], response_model=dict, status_code=200)
def create_compu(computer: Computer) -> dict:
    db = Session()
    new_computer = ComputerModel(**computer.dict())
    db.add(new_computer)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la computadora"})

@compus_router.delete('/compus/', tags=['compus'], response_model=dict, status_code=200)
def delete_compu(id: int):
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code=200, content={'message': 'Se ha eliminado la computadora'})

@compus_router.put('/compus/', tags=['compus'], response_model=dict, status_code=200)
def update_compu(id: int, compu: Computer) -> dict:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    result.marca = compu.marca
    result.modelo = compu.modelo
    result.color = compu.color
    result.ram = compu.ram
    result.almacenamiento = compu.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={'message': 'Se ha actualizado la computadora'})


