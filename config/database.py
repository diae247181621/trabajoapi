import os 
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

sqlite_file_name = "../database.sqlite"

#ESTAMOS LEYENDO EL DIRECTORIO ACTUAL QUE ES database.py
base_dir = os.path.dirname(os.path.realpath(__file__))

#CREAMOS LA URL DE LA BASE DE DATOS UNIENDO LAS 2 VARIABLES ANTERIORES
database_url= f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

engine= create_engine(database_url, echo=True)

Session = sessionmaker(bind=engine)

Base = declarative_base() 