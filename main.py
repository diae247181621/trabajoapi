# from fastapi import FastAPI, Path, Query, Request,HTTPException, Depends
# from fastapi.responses import HTMLResponse, JSONResponse
# from fastapi.security.http import HTTPAuthorizationCredentials
# from pydantic import BaseModel, Field
# from typing import  Coroutine, Optional, List
# from jwt_manager import create_token, validate_token
# from fastapi.security import HTTPBearer
# from config.database import Session, engine, Base
# from middlewares.error_handler import ErrorHandler
# from middlewares.jwt_bearer import JWTBearer
# from routers.movie import movie_router

# #TABLAS DE MI BASE DE DATOS:
# from models.compu import Computer as ComputerModel
# from models.movie import Movie as MovieModel
# from fastapi.encoders import jsonable_encoder

from fastapi.responses import HTMLResponse
from fastapi import FastAPI
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
# from routers.movie import movie_router
from routers.user import user_router
from routers.compus import compus_router

app = FastAPI()
app.title="mi primera chamba con FastAPI"
app.version="0.0.1"

app.add_middleware(ErrorHandler)
# app.include_router(movie_router)
app.include_router(user_router)
app.include_router(compus_router)

Base.metadata.create_all(bind=engine)













# #diccionario
# movies=[
#     {
#         "id":1,
#         "title":"Avatar",
#         "overview": "En un exuberante planta llamada Pandora viven monos azules",
#         "year": "2009",
#         "rating": 7.8,
#         "category": "Acción"
#     },
#     {
#         "id":2,
#         "title":"Sociedad de la nieve",
#         "overview": "Accidente de avion",
#         "year": "2024",
#         "rating": 9.8,
#         "category": "suspenso"
#     }
# ]
# compus=[
#     { # id, marca, modelo, color, ram y almacenamiento.
#         "id":1,
#         "marca":"HUAWEI",
#         "modelo": "LC9011",
#         "color":"negra",
#         "ram": "6",
#         "almacenamiento": 512,
#     }
# ]

